# jsontype: JSON Typedef validation for Golang
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/bernard-xl/jsontype.svg)](https://pkg.go.dev/gitlab.com/bernard-xl/jsontype)

This is an efficient [JSON Typedef](https://jsontypedef.com/docs/) parser and validator designed for heavy usage. It parses schemas up to 5 times faster than the alternative, with 75% less heap allocation.


# Features
- Validates already unmarshaled JSON nodes, works well with unstructured JSON parser such as [valyala/fastjson](https://github.com/valyala/fastjson)
- Parses `metadata` with user-defined parse function
- Reference to well known types, which is not defined in the schema itself
- Option to disable integer range check, to allow usage of 64-bits integers


# Installation
```
go get gitlab.com/bernard-xl/jsontype
```

# Usage
```
```