package jsontype

import (
	"time"

	"github.com/valyala/fastjson"
)

type WellKnownTypesLookupFn func(string) Form

// ValidateConfig configures a Validator.
type ValidateConfig struct {
	LookupWellKnownTypes WellKnownTypesLookupFn
	SkipIntRangeCheck    bool
	MaxDepth             int
}

// Validator encapsulates resource pool and functions to perform schema validation.
type Validator struct {
	statePool WalkStatePool
	config    ValidateConfig
}

// NewValidator instantiates a new Validator object.
func NewValidator(config ValidateConfig) *Validator {
	return &Validator{
		config: config,
	}
}

// ValidateJSON parses a JSON string and then validate it against a schema.
// This function tradeoff performance for convenience, please use ValidateInstance for better performance.
func (v *Validator) ValidateJSON(schema Schema, jsonstr string) error {
	value, err := fastjson.Parse(jsonstr)
	if err != nil {
		return fmtError(ErrInvalidInstance, "unable to parse JSON: %v", err)
	}
	return v.ValidateInstance(schema, wrapFastJSON(value))
}

// ValidateInstance validates an already parsed JSON Tree against a schema.
func (v *Validator) ValidateInstance(schema Schema, instance Node) error {
	state := v.statePool.Get()
	defer v.statePool.Put(state)

	state.Definitions = schema.Definitions
	return v.recursiveValidate(state, schema.Form, instance)
}

func (v *Validator) recursiveValidate(state *WalkState, form Form, node Node) error {
	switch form := form.(type) {
	case *EmptyForm:
		return nil
	case *TypeForm:
		return v.validateType(state, form, node)
	case *EnumForm:
		return v.validateEnum(state, form, node)
	case *ElementsForm:
		return v.validateElements(state, form, node)
	case *PropertiesForm:
		return v.validateProperties(state, form, node, nil)
	case *ValuesForm:
		return v.validateValues(state, form, node)
	case *DiscriminatorForm:
		return v.validateDiscriminator(state, form, node)
	case *RefForm:
		return v.validateRef(state, form, node)
	default:
		return fmtError(ErrMalformedSchema, "unexpected form object: %T", form)
	}
}

func (v *Validator) validateType(state *WalkState, form *TypeForm, node Node) error {
	state.SchemaPath.Push("type")
	defer state.SchemaPath.Pop()

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	switch form.Type {
	case Boolean:
		if node.Type() != True && node.Type() != False {
			return fmtInvalidErr(state, "value is not bool %q", node)
		}
		return nil
	case String:
		if node.Type() != Str {
			return fmtInvalidErr(state, "value is not string %q", node)
		}
		return nil
	case Timestamp:
		s, err := node.Str()
		if err != nil {
			return fmtInvalidErr(state, "value is not timestamp %q", node)
		}
		_, err = time.Parse(time.RFC3339, s)
		if err != nil {
			return fmtInvalidErr(state, "value is not timestamp %q", node)
		}
		return nil
	case Float32, Float64:
		_, err := node.Float()
		if err != nil {
			return fmtInvalidErr(state, "value is not float %q", node)
		}
		return nil
	case Int8, Int16, Int32, Uint8, Uint16, Uint32:
		n, err := node.Int()
		if err != nil {
			return fmtInvalidErr(state, "value is not integer %q", node)
		}

		if v.config.SkipIntRangeCheck {
			return nil
		}

		numRange, ok := numericTypeRanges[form.Type]
		if !ok {
			return fmtInvalidErr(state, "numeric range is unknown for %q", form.Type)
		}
		if int64(n) < numRange.min || int64(n) > numRange.max {
			return fmtInvalidErr(state, "value is out of range %q", n)
		}
		return nil
	default:
		return fmtError(ErrMalformedSchema, "invalid type %q", form.Type)
	}
}

func (v *Validator) validateEnum(state *WalkState, form *EnumForm, node Node) error {
	state.SchemaPath.Push("enum")
	defer state.SchemaPath.Pop()

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	s, err := node.Str()
	if err != nil {
		return fmtInvalidErr(state, "value is not string %q", node)
	}

	for _, enum := range form.Enum {
		if enum == s {
			return nil
		}
	}
	return fmtInvalidErr(state, "value is not in enum %q", s)
}

func (v *Validator) validateElements(state *WalkState, form *ElementsForm, node Node) (err error) {
	defer recoverBreak()

	state.SchemaPath.Push("elements")
	defer state.SchemaPath.Pop()

	err = v.depthCheck(state)
	if err != nil {
		return
	}

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	arr, err := node.Array()
	if err != nil {
		return fmtInvalidErr(state, "value is not array")
	}

	arr.Visit(func(idx int, child Node) {
		state.InstancePath.Push(idxStr(idx))
		err = v.recursiveValidate(state, form.Elements, child)
		state.InstancePath.Pop()

		if err != nil {
			panic(signalBreak)
		}
	})

	return nil
}

func (v *Validator) validateProperties(state *WalkState, form *PropertiesForm, node Node, ignoreKey *string) (err error) {
	defer recoverBreak()

	err = v.depthCheck(state)
	if err != nil {
		return
	}

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	obj, err := node.Object()
	if err != nil {
		return fmtInvalidErr(state, "value is not object %q", node)
	}

	requiredKey := 0
	obj.Visit(func(key string, child Node) {
		// Decides this is a "properties", "optionalProperties", or "additionalProperties",
		// and set the schema path accordingly.
		prop, ok := form.Properties[key]
		if ok {
			requiredKey += 1
			state.SchemaPath.Push("properties")
		} else {
			prop, ok = form.OptionalProperties[key]
			state.SchemaPath.Push("optionalProperties")
		}
		defer state.SchemaPath.Pop()

		if !ok {
			if ignoreKey != nil && *ignoreKey == key {
				return
			}
			if form.AdditionalProperties {
				return
			}
			err = fmtInvalidErr(state, "invalid key %q", key)
			panic(signalBreak)
		}

		state.SchemaPath.Push(key)
		state.InstancePath.Push(key)

		err = v.recursiveValidate(state, prop, child)

		state.SchemaPath.Pop()
		state.InstancePath.Pop()

		if err != nil {
			panic(signalBreak)
		}
	})

	if len(form.Properties) > requiredKey {
		// TODO: find out the missing keys
		return fmtInvalidErr(state, "missing required key(s)")
	}

	return
}

func (v *Validator) validateValues(state *WalkState, form *ValuesForm, node Node) (err error) {
	defer recoverBreak()

	state.SchemaPath.Push("values")
	defer state.SchemaPath.Pop()

	err = v.depthCheck(state)
	if err != nil {
		return
	}

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	obj, err := node.Object()
	if err != nil {
		return fmtInvalidErr(state, "value is not object %q", node)
	}

	obj.Visit(func(key string, child Node) {
		state.InstancePath.Push(key)
		err = v.recursiveValidate(state, form.Values, child)
		state.InstancePath.Pop()

		if err != nil {
			panic(signalBreak)
		}
	})
	return
}

func (v *Validator) validateDiscriminator(state *WalkState, form *DiscriminatorForm, node Node) error {
	state.SchemaPath.Push("discriminator")
	defer state.SchemaPath.Pop()

	err := v.depthCheck(state)
	if err != nil {
		return err
	}

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	obj, err := node.Object()
	if err != nil {
		if err != nil {
			return fmtInvalidErr(state, "value is not object %q", node)
		}
	}

	disc := obj.Get(form.Discriminator)
	if disc == nil {
		return fmtInvalidErr(state, "discriminator is undefined")
	}

	state.InstancePath.Push(form.Discriminator)

	discStr, err := disc.Str()
	if err != nil {
		err = fmtInvalidErr(state, "invalid discriminator %q", disc)
		state.InstancePath.Pop()
		return err
	}

	state.SchemaPath.Replace("mapping")

	mapping, ok := form.Mapping[discStr]
	if !ok {
		err = fmtInvalidErr(state, "invalid discriminator %q", discStr)
		state.InstancePath.Pop()
		return err
	}

	state.SchemaPath.Push(discStr)
	defer state.SchemaPath.Pop()

	state.InstancePath.Pop()
	return v.validateProperties(state, mapping, node, &form.Discriminator)
}

func (v *Validator) validateRef(state *WalkState, form *RefForm, node Node) error {
	err := v.depthCheck(state)
	if err != nil {
		return err
	}

	if node.Type() == Null {
		if form.Nullable {
			return nil
		}
		return fmtInvalidErr(state, "value is null")
	}

	state.SchemaPath.NewFrame()
	defer state.SchemaPath.Return()

	state.SchemaPath.Push("definitions")
	defer state.SchemaPath.Pop()

	def := state.Definitions[form.Ref]
	if def == nil && v.config.LookupWellKnownTypes != nil {
		def = v.config.LookupWellKnownTypes(form.Ref)
	}
	if def == nil {
		return fmtInvalidErr(state, "undefined defintion %q", form.Ref)
	}

	state.SchemaPath.Push(form.Ref)
	defer state.SchemaPath.Pop()

	return v.recursiveValidate(state, def, node)
}

func (v *Validator) depthCheck(state *WalkState) error {
	if v.config.MaxDepth != 0 && state.SchemaPath.Depth() > v.config.MaxDepth {
		return fmtError(ErrDepthExceeded, "depth exceeded: %v", state.SchemaPath)
	}
	return nil
}
