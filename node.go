package jsontype

// NodeType defines enumeration of node's type.
type NodeType int

const (
	Null   NodeType = 0
	Object NodeType = 1
	Array  NodeType = 2
	Str    NodeType = 3
	Number NodeType = 4
	True   NodeType = 5
	False  NodeType = 6
)

// Node is the interface that models an abstract node in parsed JSON.
type Node interface {
	Type() NodeType
	String() string
	Bool() (bool, error)
	Int() (int, error)
	Float() (float64, error)
	Str() (string, error)
	Array() (ArrayNode, error)
	Object() (ObjectNode, error)
}

// ArrayNode is the interface that models an array node in parsed JSON.
type ArrayNode interface {
	Visit(f func(idx int, value Node))
	Len() int
}

// ObjectNode is the interface that models a object node in parsed JSON.
type ObjectNode interface {
	Get(key string) Node
	Visit(f func(key string, value Node))
	Size() int
}
