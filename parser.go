package jsontype

import (
	"reflect"
	"unsafe"

	"github.com/valyala/fastjson"
)

type ParseMetadataFn func(node Node) (any, error)

// ParseConfig configures a Parser.
type ParseConfig struct {
	LookupWellKnownTypes WellKnownTypesLookupFn
	ParseMetadata        ParseMetadataFn
}

// Parser encapsulates resource pool and functions to parse JSON Typedef.
type Parser struct {
	parserPool fastjson.ParserPool
	statePool  WalkStatePool
	config     ParseConfig
}

// NewParser instantiates a new Parser object.
func NewParser(config ParseConfig) *Parser {
	return &Parser{
		config: config,
	}
}

// Parse parse the raw string into Schema object.
func (p *Parser) Parse(raw string) (s Schema, err error) {
	return p.ParseBytes(s2b(raw))
}

// ParseBytes parses the raw bytes into Schema object.
func (p *Parser) ParseBytes(raw []byte) (s Schema, err error) {
	jsonParser := p.parserPool.Get()
	defer p.parserPool.Put(jsonParser)

	state := p.statePool.Get()
	defer p.statePool.Put(state)

	value, err := jsonParser.ParseBytes(raw)
	if err != nil {
		return s, fmtError(ErrMalformedSchema, "failed to unmarshal json: %v", err)
	}

	s, err = p.parseRootSchema(value)
	if err != nil {
		return
	}

	state.Definitions = s.Definitions
	state.WellKnownTypes = p.config.LookupWellKnownTypes
	err = s.validateSelf(state)
	return
}

func (p *Parser) parseRootSchema(value *fastjson.Value) (s Schema, err error) {
	if value.Type() != fastjson.TypeObject {
		return s, newError(ErrMalformedSchema, "schema is not an object")
	}

	if def := value.Get("definitions"); def != nil {
		if def.Type() != fastjson.TypeObject {
			return s, newError(ErrMalformedSchema, "definitions is not an object")
		}

		s.Definitions, err = p.parseDefinition(def.GetObject())
		if err != nil {
			return s, err
		}
	}

	s.Form, err = p.parseForm(value, true)
	return
}

func (p *Parser) parseDefinition(defObj *fastjson.Object) (defMap map[string]Form, err error) {
	defer recoverBreak()

	defMap = make(map[string]Form, defObj.Len())

	var form Form

	defObj.Visit(func(k []byte, v *fastjson.Value) {
		form, err = p.parseForm(v, false)
		if err != nil {
			panic(signalBreak)
		}
		defMap[string(k)] = form
	})

	return defMap, err
}

func (p *Parser) parseForm(value *fastjson.Value, root bool) (form Form, err error) {
	s, err := p.parseStructure(value, root)
	if err != nil {
		return nil, err
	}

	matched := false

	if s.Type != nil {
		form, err = p.parseType(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Enum != nil {
		if matched {
			return nil, newError(ErrMalformedSchema, "conflicting field enum")
		}
		form, err = p.parseEnum(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Elements != nil {
		if matched {
			return nil, newError(ErrMalformedSchema, "conflicting field elements")
		}
		form, err = p.parseElements(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Properties != nil || s.OptionalProperties != nil || s.AdditionalProperties != nil {
		if matched {
			switch {
			case s.Properties != nil:
				return nil, newError(ErrMalformedSchema, "conflicting field props")
			case s.OptionalProperties != nil:
				return nil, newError(ErrMalformedSchema, "conflicting field optionalProps")
			default:
				return nil, newError(ErrMalformedSchema, "conflicting field additionalProps")
			}
		}

		form, err = p.parseProperties(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Values != nil {
		if matched {
			return nil, newError(ErrMalformedSchema, "conflicting field values")
		}

		form, err = p.parseValues(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Discriminator != nil || s.Mapping != nil {
		if matched {
			return nil, newError(ErrMalformedSchema, "conflicting field discriminator")
		}

		form, err = p.parseDiscriminator(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if s.Ref != nil {
		if matched {
			return nil, newError(ErrMalformedSchema, "conflicting field ref")
		}

		form, err = p.parseRef(&s)
		if err != nil {
			return
		}
		matched = true
	}

	if !matched {
		form, err = p.parseEmpty(&s)
	}
	return
}

type structured struct {
	Type                 *fastjson.Value
	Enum                 *fastjson.Value
	Elements             *fastjson.Value
	Properties           *fastjson.Value
	OptionalProperties   *fastjson.Value
	AdditionalProperties *fastjson.Value
	Values               *fastjson.Value
	Discriminator        *fastjson.Value
	Mapping              *fastjson.Value
	Ref                  *fastjson.Value
	Nullable             *fastjson.Value
	Metadata             *fastjson.Value
}

func (p *Parser) parseStructure(value *fastjson.Value, root bool) (s structured, err error) {
	obj := value.GetObject()
	if obj == nil {
		return s, newError(ErrMalformedSchema, "schema is not an object")
	}

	var unknownFields []string

	obj.Visit(func(key []byte, v *fastjson.Value) {
		// localKey MUST not escape this scope.
		localKey := b2s(key)

		switch localKey {
		case "type":
			s.Type = v
		case "enum":
			s.Enum = v
		case "elements":
			s.Elements = v
		case "properties":
			s.Properties = v
		case "optionalProperties":
			s.OptionalProperties = v
		case "additionalProperties":
			s.AdditionalProperties = v
		case "values":
			s.Values = v
		case "discriminator":
			s.Discriminator = v
		case "mapping":
			s.Mapping = v
		case "ref":
			s.Ref = v
		case "nullable":
			s.Nullable = v
		case "metadata":
			s.Metadata = v
		case "definitions":
			if !root {
				err = newError(ErrMalformedSchema, "definitions in non-root schema")
			}
		default:
			unknownFields = append(unknownFields, string(key))
		}
	})

	if unknownFields != nil {
		return s, fmtError(ErrMalformedSchema, "unknown field(s): %v", unknownFields)
	}
	return s, err
}

func (p *Parser) parseNullable(value *fastjson.Value) (bool, error) {
	if value == nil {
		return false, nil
	}

	valueType := value.Type()
	switch valueType {
	case fastjson.TypeFalse:
		return false, nil
	case fastjson.TypeTrue:
		return true, nil
	default:
		return false, newError(ErrMalformedSchema, "nullable is not bool")
	}
}

func (p *Parser) parseMetadata(value *fastjson.Value) (any, error) {
	if p.config.ParseMetadata == nil || value == nil || value.Type() == fastjson.TypeNull {
		return nil, nil
	}
	return p.config.ParseMetadata(wrapFastJSON(value))
}

func (p *Parser) parseType(s *structured) (result *TypeForm, err error) {
	result = &TypeForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	if s.Type.Type() != fastjson.TypeString {
		err = newError(ErrMalformedSchema, "type is not string")
		return
	}

	// localType MUST not escape this scope.
	localType := b2s(s.Type.GetStringBytes())

	switch localType {
	case "boolean":
		result.Type = Boolean
	case "string":
		result.Type = String
	case "timestamp":
		result.Type = Timestamp
	case "float32":
		result.Type = Float32
	case "float64":
		result.Type = Float64
	case "int8":
		result.Type = Int8
	case "uint8":
		result.Type = Uint8
	case "int16":
		result.Type = Int16
	case "uint16":
		result.Type = Uint16
	case "int32":
		result.Type = Int32
	case "uint32":
		result.Type = Uint32
	default:
		result.Type = PrimitiveType(s.Type.GetStringBytes())
	}
	return
}

func (p *Parser) parseEnum(s *structured) (result *EnumForm, err error) {
	result = &EnumForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	if s.Enum.Type() != fastjson.TypeArray {
		err = newError(ErrMalformedSchema, "enum is not array of string")
		return
	}

	array := s.Enum.GetArray()
	result.Enum = make([]string, len(array))
	for i, v := range array {
		if v.Type() != fastjson.TypeString {
			err = newError(ErrMalformedSchema, "enum is not array of string")
			return
		}
		result.Enum[i] = string(v.GetStringBytes())
	}
	return
}

func (p *Parser) parseElements(s *structured) (result *ElementsForm, err error) {
	result = &ElementsForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	result.Elements, err = p.parseForm(s.Elements, false)
	return
}

func (p *Parser) parseProperties(s *structured) (result *PropertiesForm, err error) {
	defer recoverBreak()

	result = &PropertiesForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	if s.Properties != nil {
		if s.Properties.Type() != fastjson.TypeObject {
			err = newError(ErrMalformedSchema, "properties is not an object")
			return
		}

		obj := s.Properties.GetObject()
		result.Properties = make(map[string]Form, obj.Len())

		obj.Visit(func(key []byte, v *fastjson.Value) {
			result.Properties[string(key)], err = p.parseForm(v, false)
			if err != nil {
				panic(signalBreak)
			}
		})
	}

	if s.OptionalProperties != nil {
		if s.OptionalProperties.Type() != fastjson.TypeObject {
			err = newError(ErrMalformedSchema, "optionalProperties is not an object")
			return
		}

		obj := s.OptionalProperties.GetObject()
		result.OptionalProperties = make(map[string]Form, obj.Len())

		obj.Visit(func(key []byte, v *fastjson.Value) {
			result.OptionalProperties[string(key)], err = p.parseForm(v, false)
			if err != nil {
				panic(signalBreak)
			}
		})
	}

	if s.AdditionalProperties != nil {
		additionalType := s.AdditionalProperties.Type()
		switch additionalType {
		case fastjson.TypeFalse:
			result.AdditionalProperties = false
		case fastjson.TypeTrue:
			result.AdditionalProperties = true
		default:
			err = newError(ErrMalformedSchema, "additionalProperties is not bool")
		}

	}
	return
}

func (p *Parser) parseValues(s *structured) (result *ValuesForm, err error) {
	result = &ValuesForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	result.Values, err = p.parseForm(s.Values, false)
	return
}

func (p *Parser) parseDiscriminator(s *structured) (result *DiscriminatorForm, err error) {
	defer recoverBreak()

	result = &DiscriminatorForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	if s.Discriminator != nil {
		if s.Discriminator.Type() != fastjson.TypeString {
			err = newError(ErrMalformedSchema, "discriminator is not string")
			return
		}
		result.Discriminator = string(s.Discriminator.GetStringBytes())
	}

	if s.Mapping != nil {
		if s.Mapping.Type() != fastjson.TypeObject {
			err = newError(ErrMalformedSchema, "mapping is not an object")
			return
		}

		obj := s.Mapping.GetObject()
		result.Mapping = make(map[string]*PropertiesForm, obj.Len())

		var form Form

		obj.Visit(func(key []byte, v *fastjson.Value) {
			form, err = p.parseForm(v, false)
			if err != nil {
				panic(signalBreak)
			}

			props, ok := form.(*PropertiesForm)
			if !ok {
				err = newError(ErrMalformedSchema, "mapping is not in properties form")
				panic(signalBreak)
			}

			result.Mapping[string(key)] = props
		})
	}
	return
}

func (p *Parser) parseRef(s *structured) (result *RefForm, err error) {
	result = &RefForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	if err != nil {
		return
	}

	if s.Ref.Type() != fastjson.TypeString {
		err = newError(ErrMalformedSchema, "ref is not string")
		return
	}

	result.Ref = string(s.Ref.GetStringBytes())
	return
}

func (p *Parser) parseEmpty(s *structured) (result *EmptyForm, err error) {
	result = &EmptyForm{}

	result.Nullable, err = p.parseNullable(s.Nullable)
	if err != nil {
		return
	}

	result.Metadata, err = p.parseMetadata(s.Metadata)
	return
}

func b2s(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

func s2b(s string) (b []byte) {
	strh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	sh := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh.Data = strh.Data
	sh.Len = strh.Len
	sh.Cap = strh.Len
	return b
}
