package jsontype

import (
	"strconv"
	"strings"
	"sync"
)

// WalkState carries the necessary context when walking through the schema.
type WalkState struct {
	Definitions    map[string]Form
	WellKnownTypes WellKnownTypesLookupFn
	SchemaPath     PathBuilder
	InstancePath   PathBuilder
}

// Reset prepares the object for reuse, which is helpful in improving performance.
func (v *WalkState) Reset() {
	v.Definitions = nil
	v.SchemaPath.Reset()
	v.InstancePath.Reset()
}

// WalkStatePool is a typed wrapper over sync.Pool.
type WalkStatePool struct {
	pool sync.Pool
}

// Get retrieves an already allocate WalkState from the pool,
// fallback to allocate a new WalkState when that is impossible.
func (w *WalkStatePool) Get() *WalkState {
	vs := w.pool.Get()
	if vs == nil {
		return &WalkState{}
	}
	return vs.(*WalkState)
}

// Put returns a WalkState to the pool.
func (w *WalkStatePool) Put(vs *WalkState) {
	vs.Reset()
	w.pool.Put(vs)
}

// PathBuilder provides functions to maintain the path efficiently when walking through schema or instance.
// It maintains path component in a stack, and uses frame to differentiates multiple paths.
type PathBuilder struct {
	components []string
	frames     []int
}

// Push appends a path component to the end in the current frame.
func (p *PathBuilder) Push(c string) {
	p.components = append(p.components, c)
}

// Pop discards a path component at the end in the current frame.
// Please ensure the stack is non empty before calling this function.
func (p *PathBuilder) Pop() {
	pathLen := len(p.components)
	p.components = p.components[:pathLen-1]
}

// Replace replaces the last component in the stack with the provided one.
// It is an optimised way to pop away a component and then immediately push a component.
func (p *PathBuilder) Replace(c string) {
	pathLen := len(p.components)
	p.components[pathLen-1] = c
}

// NewFrame starts a new frame, while remembers the old path which can be recovered later with the Return function.
// This is analogous to starting a new stack frame in function call.
func (p *PathBuilder) NewFrame() {
	pathLen := len(p.components)
	p.frames = append(p.frames, pathLen)
}

// Return discards the current frame (and all path components in it), and recovers the previous frame.
func (p *PathBuilder) Return() {
	lastCheckpt := len(p.frames)
	p.frames = p.frames[:lastCheckpt-1]
}

// Depth reports the total number of path components in all frames.
// The depth is unaffected by NewFrame and Return.
func (p *PathBuilder) Depth() int {
	return len(p.components)
}

// Reset prepares the object for reuse, which is helpful in improving performance.
func (p *PathBuilder) Reset() {
	p.components = p.components[:0]
	p.frames = p.frames[:0]
}

// Path returns the path components in current frame.
func (p *PathBuilder) Path() []string {
	if len(p.frames) == 0 {
		return p.components
	}
	lastCheckPt := p.frames[len(p.frames)-1]
	return p.components[lastCheckPt:]
}

// String concatenataes all path components in current frame into a string.
func (p *PathBuilder) String() string {
	lastCheckPt := p.frames[len(p.frames)-1]
	return strings.Join(p.components[lastCheckPt:], "/")
}

var idxTab = [...]string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}

func idxStr(idx int) string {
	if idx >= 0 && idx <= 9 {
		return idxTab[idx]
	}
	return strconv.FormatInt(int64(idx), 10)
}
