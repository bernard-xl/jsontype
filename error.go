package jsontype

import (
	"errors"
	"fmt"
)

// Error carries context specific information in additional to the error message.
// All API of this package shall return this error, it can be unwrapped into its cause.
type Error struct {
	schemaPath   []string
	instancePath []string
	msg          string
	cause        error
}

// SchemaPath tells which part of the JSON Typedef schema caused this error.
func (e *Error) SchemaPath() []string {
	return e.schemaPath
}

// InstancePath tells which part of the JSON instance caused this error.
func (e *Error) InstancePath() []string {
	return e.instancePath
}

// Error returns a simple error message.
func (e *Error) Error() string {
	return e.msg
}

// Unwrap enables usage of [errors.Is] to find out the wrapped sentinel error.
func (e *Error) Unwrap() error {
	return e.cause
}

func newError(err error, msg string) *Error {
	return &Error{
		msg:   "jsontype: " + msg,
		cause: err,
	}
}

func fmtError(err error, format string, args ...any) *Error {
	return &Error{
		msg:   fmt.Sprintf("jsontype: "+format, args...),
		cause: err,
	}
}

func newMalformedErr(state *WalkState, msg string) *Error {
	return &Error{
		schemaPath:   state.SchemaPath.Path(),
		instancePath: state.InstancePath.Path(),
		msg:          "jsontype: " + msg,
		cause:        ErrMalformedSchema,
	}
}

func fmtInvalidErr(state *WalkState, format string, args ...any) *Error {
	return &Error{
		schemaPath:   state.SchemaPath.Path(),
		instancePath: state.InstancePath.Path(),
		msg:          fmt.Sprintf("jsontype: "+format, args...),
		cause:        ErrInvalidInstance,
	}
}

// Sentinel error values, they are wrapped by [Error] as the cause of error.
var (
	ErrMalformedSchema = errors.New("malformed schema")
	ErrInvalidInstance = errors.New("invalid instance")
	ErrDepthExceeded   = errors.New("recursion depth exceeded")
	ErrWrongType       = errors.New("wrong type")
)

var signalBreak = errors.New("break from visit")

func recoverBreak() {
	r := recover()
	if r != nil && r != signalBreak {
		panic(r)
	}
}
