package jsontype

// Schema models a complete unit of schema.
type Schema struct {
	Form
	Definitions map[string]Form
}

// PrimitiveType defines enumeration of JTD primitive types.
type PrimitiveType string

const (
	Boolean   PrimitiveType = "boolean"
	String    PrimitiveType = "string"
	Timestamp PrimitiveType = "timestamp"
	Float32   PrimitiveType = "float32"
	Float64   PrimitiveType = "float64"
	Int8      PrimitiveType = "int8"
	Uint8     PrimitiveType = "uint8"
	Int16     PrimitiveType = "int16"
	Uint16    PrimitiveType = "uint16"
	Int32     PrimitiveType = "int32"
	Uint32    PrimitiveType = "uint32"
)

type intRange struct {
	min int64
	max int64
}

var numericTypeRanges = map[PrimitiveType]intRange{
	Int8:   {min: -128, max: 127},
	Uint8:  {min: 0, max: 255},
	Int16:  {min: -32768, max: 32767},
	Uint16: {min: 0, max: 65535},
	Int32:  {min: -2_147_483_648, max: 2_147_483_647},
	Uint32: {min: 0, max: 4_294_967_295},
}

// Form is implemented by all schema forms.
type Form interface {
	validateSelf(state *WalkState) error
}

// EmptyForm describes instances whose values are unknown, unpredictable, or otherwise unconstrained by the schema.
type EmptyForm struct {
	Nullable bool `json:"nullable,omitempty"`
	Metadata any  `json:"metadata,omitempty"`
}

func (*EmptyForm) validateSelf(state *WalkState) error {
	return nil
}

// TypeForm describes instances whose value is a boolean, number, string, or RFC3339 timestamp.
type TypeForm struct {
	Type     PrimitiveType `json:"type,omitempty"`
	Nullable bool          `json:"nullable,omitempty"`
	Metadata any           `json:"metadata,omitempty"`
}

func (t *TypeForm) validateSelf(state *WalkState) error {
	switch t.Type {
	case Boolean, String, Timestamp, Float32, Float64, Int8, Uint8, Int16, Uint16, Int32, Uint32:
		return nil
	default:
		return fmtInvalidErr(state, "invalid type %q", t.Type)
	}
}

// EnumForm describes instances whose value must be one of a given set of string values.
type EnumForm struct {
	Enum     []string `json:"enum"`
	Nullable bool     `json:"nullable,omitempty"`
	Metadata any      `json:"metadata,omitempty"`
}

func (e *EnumForm) validateSelf(state *WalkState) error {
	if len(e.Enum) == 0 {
		return newMalformedErr(state, "enum is empty")
	}

	enumSet := make(map[string]struct{}, len(e.Enum))

	for _, enum := range e.Enum {
		_, found := enumSet[enum]
		if found {
			return fmtInvalidErr(state, "duplicated enum %q", enum)
		}
		enumSet[enum] = struct{}{}
	}
	return nil
}

// ElementsForm describes instances that must be arrays. A further subschema describes the elements of the array.
type ElementsForm struct {
	Elements Form `json:"elements"`
	Nullable bool `json:"nullable,omitempty"`
	Metadata any  `json:"metadata,omitempty"`
}

func (e *ElementsForm) validateSelf(state *WalkState) error {
	return e.Elements.validateSelf(state)
}

// PropertiesForm describes JSON objects being used as a "struct".
type PropertiesForm struct {
	Properties           map[string]Form `json:"properties,omitempty"`
	OptionalProperties   map[string]Form `json:"optionalProperties,omitempty"`
	AdditionalProperties bool            `json:"additionalProperties,omitempty"`
	Nullable             bool            `json:"nullable,omitempty"`
	Metadata             any             `json:"metadata,omitempty"`
}

func (p *PropertiesForm) validateSelf(state *WalkState) error {
	if len(p.Properties) == 0 && len(p.OptionalProperties) == 0 {
		return newMalformedErr(state, "both properties and optionalProperties are empty")
	}

	for propName := range p.Properties {
		_, found := p.OptionalProperties[propName]
		if found {
			return fmtInvalidErr(state, "%q in both properties and optionalProperties", propName)
		}
	}

	for propName, form := range p.Properties {
		state.SchemaPath.Push(propName)
		err := form.validateSelf(state)
		state.SchemaPath.Pop()
		if err != nil {
			return err
		}
	}

	for propName, form := range p.OptionalProperties {
		state.SchemaPath.Push(propName)
		err := form.validateSelf(state)
		state.SchemaPath.Pop()
		if err != nil {
			return err
		}
	}

	return nil
}

// ValuesForm describes instances that are JSON objects being used as an associative array.
type ValuesForm struct {
	Values   Form `json:"values"`
	Nullable bool `json:"nullable,omitempty"`
	Metadata any  `json:"metadata,omitempty"`
}

func (v *ValuesForm) validateSelf(state *WalkState) error {
	if v.Values == nil {
		return newMalformedErr(state, "values is empty")
	}

	return v.Values.validateSelf(state)
}

// DiscriminatorForm describes JSON objects being used in a fashion similar to a discriminated union construct in C-like languages.
type DiscriminatorForm struct {
	Discriminator string                     `json:"discriminator"`
	Mapping       map[string]*PropertiesForm `json:"mapping"`
	Nullable      bool                       `json:"nullable,omitempty"`
	Metadata      any                        `json:"metadata,omitempty"`
}

func (d *DiscriminatorForm) validateSelf(state *WalkState) error {
	if d.Discriminator == "" {
		return newMalformedErr(state, "discriminator is empty")
	}

	if d.Mapping == nil {
		return newMalformedErr(state, "mapping is undefined")
	}

	for _, m := range d.Mapping {
		if m.Nullable {
			return newMalformedErr(state, "mapping is nullable")
		}

		for p := range m.Properties {
			if d.Discriminator == p {
				return fmtInvalidErr(state, "discriminator shares key %q with mapping properties", p)
			}
		}

		for op := range m.OptionalProperties {
			if d.Discriminator == op {
				return fmtInvalidErr(state, "discriminator shares key %q with mapping optionalProperties", op)
			}
		}

		err := m.validateSelf(state)
		if err != nil {
			return err
		}
	}
	return nil
}

// RefForm is for when a schema is defined in terms of something in the "definitions" of the root schema.
// It enables schemas to be less repetitive and also enables describing recursive structures.
type RefForm struct {
	Ref      string `json:"ref"`
	Nullable bool   `json:"nullable,omitempty"`
	Metadata any    `json:"metadata,omitempty"`
}

func (r *RefForm) validateSelf(state *WalkState) error {
	_, defined := state.Definitions[r.Ref]

	wellKnown := false
	if !defined && state.WellKnownTypes != nil {
		wellKnown = state.WellKnownTypes(r.Ref) != nil
	}

	if !(defined || wellKnown) {
		return fmtInvalidErr(state, "undefined defintion %q", r.Ref)
	}
	return nil
}
