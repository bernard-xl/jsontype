package jsontype

import "github.com/valyala/fastjson"

type fastjsonNode struct {
	value *fastjson.Value
}

func wrapFastJSON(value *fastjson.Value) fastjsonNode {
	return fastjsonNode{value}
}

func (f fastjsonNode) Type() NodeType {
	return NodeType(f.value.Type())
}

func (f fastjsonNode) String() string {
	return f.value.String()
}

func (f fastjsonNode) Bool() (bool, error) {
	return f.value.Bool()
}

func (f fastjsonNode) Int() (int, error) {
	return f.value.Int()
}

func (f fastjsonNode) Float() (float64, error) {
	return f.value.Float64()
}

func (f fastjsonNode) Str() (string, error) {
	strBytes, err := f.value.StringBytes()
	if err != nil {
		return "", err
	}
	return b2s(strBytes), nil
}

func (f fastjsonNode) Array() (ArrayNode, error) {
	array, err := f.value.Array()
	if err != nil {
		return nil, err
	}
	return &fastJSONArray{array}, nil
}

func (f fastjsonNode) Object() (ObjectNode, error) {
	obj, err := f.value.Object()
	if err != nil {
		return nil, err
	}
	return fastJSONObject{obj}, err
}

type fastJSONArray struct {
	array []*fastjson.Value
}

func (f fastJSONArray) Visit(fn func(int, Node)) {
	for idx, value := range f.array {
		instance := fastjsonNode{value}
		fn(idx, instance)
	}
}

func (f fastJSONArray) Len() int {
	return len(f.array)
}

type fastJSONObject struct {
	obj *fastjson.Object
}

func (f fastJSONObject) Get(key string) Node {
	value := f.obj.Get(key)
	if value == nil {
		return nil
	}
	return fastjsonNode{value}
}

func (f fastJSONObject) Visit(fn func(key string, value Node)) {
	f.obj.Visit(func(key []byte, v *fastjson.Value) {
		fn(b2s(key), fastjsonNode{v})
	})
}

func (f fastJSONObject) Size() int {
	return f.obj.Len()
}
