package jsontype

import (
	"encoding/json"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/valyala/fastjson"
)

func TestParseMetadata(t *testing.T) {
	testdata, err := os.ReadFile("testdata/metadata_schema.json")
	require.NoError(t, err)

	parseMeta := func(node Node) (any, error) {
		obj, err := node.Object()
		if err != nil {
			return nil, err
		}

		meta := &testingMetadata{}
		obj.Visit(func(key string, value Node) {
			if key == "default" {
				meta.Default, err = value.Str()
			}
		})
		return meta, err
	}

	schemaParser := NewParser(ParseConfig{
		ParseMetadata: parseMeta,
	})

	s, err := schemaParser.ParseBytes(testdata)
	require.NoError(t, err)
	require.IsType(t, &PropertiesForm{}, s.Form)

	prop := s.Form.(*PropertiesForm)
	require.Contains(t, prop.Properties, "foo")
	require.IsType(t, &TypeForm{}, prop.Properties["foo"])

	foo := prop.Properties["foo"].(*TypeForm)
	require.IsType(t, &testingMetadata{}, foo.Metadata)

	meta := foo.Metadata.(*testingMetadata)
	require.Equal(t, "bar", meta.Default)
}

func TestValidSchema(t *testing.T) {
	testdata, err := os.ReadFile("testdata/valid_schema.json")
	require.NoError(t, err)

	var validCases map[string]struct {
		Skip     bool            `json:"skip"`
		Schema   json.RawMessage `json:"schema"`
		Instance json.RawMessage `json:"instance"`
		Errors   []struct {
			InstancePath []string `json:"instancePath"`
			SchemaPath   []string `json:"schemaPath"`
		} `json:"errors"`
	}
	err = json.Unmarshal(testdata, &validCases)
	require.NoError(t, err)

	instanceParser := &fastjson.Parser{}
	schemaParser := NewParser(ParseConfig{})

	validator := NewValidator(ValidateConfig{})

	for caseName, testCase := range validCases {
		t.Run(caseName, func(t *testing.T) {
			if testCase.Skip {
				t.Skip()
			}

			value, err := instanceParser.ParseBytes(testCase.Instance)
			require.NoError(t, err)

			schema, err := schemaParser.ParseBytes(testCase.Schema)
			require.NoError(t, err)

			err = validator.ValidateInstance(schema, wrapFastJSON(value))

			if len(testCase.Errors) == 0 {
				require.NoError(t, err)
				return
			}

			require.IsType(t, &Error{}, err)
			if strings.Contains(caseName, "properties") || strings.Contains(caseName, "Properties") {
				return
			}
			typedErr := err.(*Error)

			expectedErr := testCase.Errors[0]
			require.Equal(t, patchEmptyPath(expectedErr.SchemaPath), patchEmptyPath(typedErr.SchemaPath()))
			require.Equal(t, patchEmptyPath(expectedErr.InstancePath), patchEmptyPath(typedErr.InstancePath()))
		})
	}
}

func TestInvalidSchema(t *testing.T) {
	testdata, err := os.ReadFile("testdata/invalid_schema.json")
	require.NoError(t, err)

	var invalidCases map[string]json.RawMessage
	err = json.Unmarshal(testdata, &invalidCases)
	require.NoError(t, err)

	parser := NewParser(ParseConfig{})

	for caseName, testCase := range invalidCases {
		t.Run(caseName, func(t *testing.T) {
			_, err := parser.ParseBytes(testCase)
			require.Error(t, err)
		})
	}
}

func BenchmarkValidSchema(b *testing.B) {
	testdata, err := os.ReadFile("testdata/valid_schema.json")
	require.NoError(b, err)

	var validCases map[string]struct {
		Schema json.RawMessage `json:"schema"`
	}
	err = json.Unmarshal(testdata, &validCases)
	require.NoError(b, err)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		parser := NewParser(ParseConfig{})

		for _, testCase := range validCases {
			_, err := parser.ParseBytes(testCase.Schema)
			if err != nil {
				b.Fatal(err)
			}
		}
	}
}

func patchEmptyPath(p []string) []string {
	if len(p) == 0 {
		return nil
	}
	return p
}

type testingMetadata struct {
	Default string `json:"default,omitempty"`
}
